//
//  AuthorizationViewController.swift
//  remonline
//
//  Created by Рамиз Кичибеков on 14/08/2017.
//  Copyright © 2017 Рамиз Кичибеков. All rights reserved.
//

import UIKit

final class AuthorizationViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var topPostion: NSLayoutConstraint!
    @IBOutlet weak var intialTopPosition: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        subscribeNotification()
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configureTextFields()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        closeKeyboard()
    }
    
    //MARK: - Custom method's
    
    private func subscribeNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: Notification.Name.UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: Notification.Name.UIKeyboardDidHide,
                                               object: nil)
    }
    
    private func configureTextFields() {
        for textField in [emailTextField, passwordTextField] {
            let border = CALayer()
            let width: CGFloat = 1.0
            border.borderColor = UIColor.applicationGreenColor.cgColor
            border.frame = CGRect(x: 0, y: (textField?.frame.size.height)! - width, width:  (textField?.frame.size.width)!, height: (textField?.frame.size.height)!)
            
            border.borderWidth = width
            textField?.layer.addSublayer(border)
            textField?.layer.masksToBounds = true
        }
    }
    
    private func configureAuthorized() {
        RemonlineService.shared.authorizedByEmailAndPassword((emailTextField.text)!,
                                                             password: (passwordTextField.text)!) { (token, error) in
                                                                guard let error = error else {
                                                                    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                                    UIApplication.shared.keyWindow?.rootViewController = mainStoryboard.instantiateInitialViewController()
                                                                    return
                                                                }
                                                                self.errorAlertWith(error.localizedDescription,
                                                                                    actionBlock: { (alertController) in
                                                                                        let agreeAction = UIAlertAction(title: "ОК",
                                                                                                                        style: .default,
                                                                                                                        handler: nil)
                                                                                        alertController.addAction(agreeAction)
                                                                })
        }
    }
    
    private func closeKeyboard() {
        UIView.animate(withDuration: 0.7,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.5,
                       options: [.beginFromCurrentState, .curveEaseOut],
                       animations: {
                        self.topPostion.isActive = false
                        self.intialTopPosition.isActive = true
                        self.view.layoutIfNeeded()
        }) { (finished) in
            
        }
    }
    
    //MARK: - Notification
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! CGRect).size.height
        UIView.animate(withDuration: 0.7,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.5,
                       options: [.beginFromCurrentState, .curveEaseOut],
                       animations: {
                        self.topPostion.isActive = true
                        self.intialTopPosition.isActive = false
                        self.topPostion.constant = keyboardHeight - self.intialTopPosition.constant
                        self.view.layoutIfNeeded()
        }) { (finished) in
            
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if emailTextField.resignFirstResponder() || passwordTextField.resignFirstResponder() {
            UIView.animate(withDuration: 0.7,
                           delay: 0,
                           usingSpringWithDamping: 0.5,
                           initialSpringVelocity: 0.5,
                           options: [.beginFromCurrentState, .curveEaseOut],
                           animations: {
                            self.topPostion.constant = self.intialTopPosition.constant
                            self.view.layoutIfNeeded()
            }) { (finished) in
                
            }
        }
    }
    
    //MARK: - Action's
    
    @IBAction private func didTapSignInButton(_ sender: UIButton) {
        configureAuthorized()
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            if !(textField.text?.isEmpty)! {
                let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
                let predicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
                return predicate.evaluate(with:textField.text)
            }
        }
        return true
    }
    
}
