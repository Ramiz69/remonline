//
//  NetworkService.swift
//  remonline
//
//  Created by Рамиз Кичибеков on 23/07/2017.
//  Copyright © 2017 Рамиз Кичибеков. All rights reserved.
//

import Foundation

final class RemonlineService {
    
    enum RemonlineError: Error {
        case invalidToken
    }
    
    typealias complectionHandler = (Any?, Error?) -> ()
    
    private enum AuthorizationMethod: String {
        case byEmailAndPassword = "token/by-login"
        case byAPIKey = "token/new"
    }
    
    private let getOrderMethod = "order/"
    
    private let apiKey = "8a3982c17d6044f38a8530337425c27e"
    private lazy var session = { () -> URLSession in 
        let localSession = URLSession.shared
        return localSession
    }()
    
    static let shared = {
        RemonlineService()
    }()
    
    private init() {
        
    }
    ///Авторизация
    open func authorizedByEmailAndPassword(_ email: String,
                                           password: String,
                                           complectionHandler: @escaping complectionHandler) {
        let request = URLRequest.postURLRequest(AuthorizationMethod.byEmailAndPassword.rawValue,
                                                parameters: "login=\(email)&password=\(password)")
        session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    complectionHandler(nil, error)
                }
            } else {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                    if json?["code"] != nil && json?["message"] != nil {
                        DispatchQueue.main.async {
                            complectionHandler(nil, error)
                        }
                    } else {
                        DispatchQueue.main.async {
                            KeychainManager.saveToken(json?["token"] as! String)
                            complectionHandler(json?["token"], nil)
                        }
                    }
                } catch {
                    DispatchQueue.main.async {
                        complectionHandler(nil, error)
                    }
                }
            }
        }.resume()
    }
    
    open func authorizedByAPIKey(_ complectionHandler: @escaping complectionHandler)  {
        let request = URLRequest.postURLRequest(AuthorizationMethod.byAPIKey.rawValue, parameters: "api_key=\(apiKey)")
        session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    complectionHandler(nil, error)
                }
            } else {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                    DispatchQueue.main.async {
                        KeychainManager.saveToken(json?["token"] as! String)
                        complectionHandler(json?["token"], nil)
                    }
                } catch {
                    DispatchQueue.main.async {
                        complectionHandler(nil, error)
                    }
                }
            }
        }.resume()
    }
    ///Получаем список заказов
    open func getOrderList(_ page: NSNumber = 1, complectionHandler: @escaping complectionHandler) {
        let request = URLRequest.getURLRequest(getOrderMethod, parameters: ["token" : KeychainManager.getToken() as AnyObject, "page" : page])
        session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    complectionHandler(nil, error)
                }
            } else {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String : Any]
                    print("JSON = \(String(describing: json?["data"]))")
                    if json?["code"] != nil && json?["message"] != nil {
                        DispatchQueue.main.async {
                            //Переавторизоваться
                        }
                    } else {
                        var ordersArray = [OrderEntity]()
                        for dictionary in json?["data"] as! [[String : Any]] {
                            let order = OrderEntity.createOrUpdateOrderWith(dictionary)
                            ordersArray.append(order!)
                        }
                        DispatchQueue.main.async {
                            complectionHandler(ordersArray, nil)
                        }
                    }
                } catch {
                    DispatchQueue.main.async {
                        complectionHandler(nil, error)
                    }
                }
            }
        }.resume()
    }
    
}
