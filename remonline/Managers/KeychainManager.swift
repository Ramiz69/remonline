//
//  KeychainManager.swift
//  remonline
//
//  Created by Рамиз Кичибеков on 23/07/2017.
//  Copyright © 2017 Рамиз Кичибеков. All rights reserved.
//

import Foundation
import Security

private let tokenKey = "TokenKey"
private let userAccount = "AuthorizationUser"

final class KeychainManager: NSObject {
    
    open class func saveToken(_ token: String!) {
        save(tokenKey, data: token)
    }
    
    open class func getToken() -> String? {
        return loadToken(tokenKey)
    }
    
    private class func save(_ key: String!, data: String!) {
        let dataFromString = data.data(using: .utf8, allowLossyConversion: false)!
        
        let keychainQuery = [kSecClass as String : kSecClassGenericPassword as String,
                             kSecAttrAccount as String : key,
                             kSecValueData as String : dataFromString ] as [String : Any]
        SecItemDelete(keychainQuery as CFDictionary)
        SecItemAdd(keychainQuery as CFDictionary, nil)
    }
    
    private class func loadToken(_ key: String!) -> String? {
        let keychainQuery = [kSecClass as String : kSecClassGenericPassword,
                             kSecAttrAccount as String : key,
                             kSecReturnData as String : kCFBooleanTrue,
                             kSecMatchLimit as String : kSecMatchLimitOne ] as [String : Any]
        
        var dataTypeRef: AnyObject? = .none
        
        let status = SecItemCopyMatching(keychainQuery as CFDictionary, &dataTypeRef)
        var contentsOfKeychain: String? = .none
        
        if status == noErr {
            if let data = dataTypeRef as? Data {
                contentsOfKeychain = String.init(data: data, encoding: .utf8)
            }
        } else {
            print("Nothing was retrieved from the keychain. Status code \(status)")
        }
        
        return contentsOfKeychain
    }
    
    private class func removeToken(_ key: String) {
        
        let keychainQuery = [kSecClass as String : kSecClassGenericPassword,
                             kSecAttrAccount as String : key,
                             kSecReturnData as String : kCFBooleanTrue,
                             kSecMatchLimit as String : kSecMatchLimitOne] as [String : Any]
        
        var dataTypeRef: AnyObject? = .none
        
        let status = SecItemCopyMatching(keychainQuery as CFDictionary, &dataTypeRef)
        
        if (status != errSecSuccess) {
            print("Keychain is clear. Status \(status)")
        }
        
    }
}
