//
//  CoreDataManager.swift
//  remonline
//
//  Created by Рамиз Кичибеков on 30/07/2017.
//  Copyright © 2017 Рамиз Кичибеков. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataManager {
    
    open var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
    
    static let shared: CoreDataManager = {
        return CoreDataManager()
    }()
    
    private init() {
        managedObjectContext = persistentContainer.viewContext
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "remonline")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    open func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    open func deleteDataFrom(entity: String) {
        let context = CoreDataManager.shared.managedObjectContext
        let entityDescription = NSEntityDescription.entity(forEntityName: entity, in: context)
        let request = NSFetchRequest<NSFetchRequestResult>()
        request.entity = entityDescription
        request.includesPropertyValues = false
        
        do {
            guard let results = try context.fetch(request) as [AnyObject]? else { return }
            
            for object in results {
                context.delete(object as! NSManagedObject)
            }
        } catch {
            print("Error = \(error.localizedDescription)")
        }
    }
}
