//
//  OrderEntity.swift
//  remonline
//
//  Created by Рамиз Кичибеков on 31/07/2017.
//  Copyright © 2017 Рамиз Кичибеков. All rights reserved.
//

import UIKit
import CoreData

@objc(OrderEntity)
final class OrderEntity: NSManagedObject {
    
    @NSManaged var orderID: NSNumber
    @NSManaged var numberOrder: String
    @NSManaged var device: String
    @NSManaged var faultDevice: String
    @NSManaged var statusOrder: Any
    
    open class func createOrUpdateOrderWith(_ json: [String : Any], context: NSManagedObjectContext = DataStore.localDataBase.managedObjectContext) -> OrderEntity? {
        var orderEntity: OrderEntity? = .none
        let predicate = NSPredicate(format: "orderID == %@", json["id"] as! CVarArg)
        orderEntity = findOrCreateWith(context, predicate: predicate) as? OrderEntity
        guard let order = orderEntity else { return nil }
        order.orderID = json["id"] as! NSNumber
        order.numberOrder = json["id_label"] as! String
        order.device = "\(json["brand"]!)\(json["model"]!)"
        order.faultDevice = json["malfunction"] as! String
        order.statusOrder = NSKeyedArchiver.archivedData(withRootObject: json["status"]!)
        
        DataStore.saveDataBase()
        
        return order
    }
    
}
