//
//  DataStore.swift
//  remonline
//
//  Created by Рамиз Кичибеков on 30/07/2017.
//  Copyright © 2017 Рамиз Кичибеков. All rights reserved.
//

import Foundation

final class DataStore {
    
    open class var localDataBase: CoreDataManager {
        return CoreDataManager.shared
    }
    
    open class func saveDataBase() {
        CoreDataManager.shared.saveContext()
    }
    
    open class func deleteDataFrom(_ entity: String) {
        CoreDataManager.shared.deleteDataFrom(entity: entity)
    }

}
