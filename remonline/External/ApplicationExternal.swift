//
//  ApplicationExternak.swift
//  remonline
//
//  Created by Рамиз Кичибеков on 23/07/2017.
//  Copyright © 2017 Рамиз Кичибеков. All rights reserved.
//

import UIKit
import CoreData

extension UIViewController {
    typealias AlertActionBlock = (UIAlertController) -> ()
    
    func alertWith(_ title: String?, message: String?, actionBlock: AlertActionBlock) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        alertController.view.tintColor = view.tintColor
        actionBlock(alertController)
        present(alertController, animated: true, completion: nil)
    }
    
    func errorAlertWith(_ message: String?, actionBlock: AlertActionBlock) {
        let alertController = UIAlertController(title: "Ошибка",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.view.tintColor = view.tintColor
        actionBlock(alertController)
        present(alertController, animated: true, completion: nil)
    }
}

extension MainViewController {
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        
        if deltaOffset == 0 {
            loadMoreOrders()
        }
    }
}

extension URLRequest {
    
    static func postURLRequest(_ methodURL: String, parameters: String) -> URLRequest {
        var request = URLRequest(url: URL(string: "https://api.remonline.ru/" + methodURL)!,
                                 cachePolicy: .reloadIgnoringLocalCacheData,
                                 timeoutInterval: 60)
        request.httpMethod = "POST"
        request.httpBody = parameters.data(using: .utf8)
        
        return request
    }
    
    static func getURLRequest(_ methodURL: String, parameters: [String : AnyObject]?) -> URLRequest {
        let serverURL = "https://api.remonline.ru/" + formatingURL(methodURL, parameters: parameters)
        var request = URLRequest(url: URL(string: serverURL)!,
                                 cachePolicy: .reloadIgnoringLocalCacheData,
                                 timeoutInterval: 60)
        request.httpMethod = "GET"
        
        return request
    }
    
    private static func formatingURL(_ method: String, parameters: [String : AnyObject]?) -> String {
        var text = ""
        if let dictionary = parameters {
            for (key, value) in dictionary {
                text += (text.isEmpty ? "?" : "&") + key + "=" + "\(value)"
            }
        }
        return method + text
    }
}

extension NSManagedObject {
    
    class func findOrCreateWith(_ context: NSManagedObjectContext, predicate: NSPredicate) -> NSManagedObject? {
        var object: NSManagedObject? = .none
        object = findObjectWith(context,
                                predicate: predicate,
                                error: nil)
        if object == nil {
            object = NSEntityDescription.insertNewObject(forEntityName: String(describing: self), into: context)
        }
        return object
    }
    
    class func findObjectWith(_ context: NSManagedObjectContext, predicate: NSPredicate, error: Error?) -> NSManagedObject? {
        let fetchRequest = createFetchRequest()
        fetchRequest.predicate = predicate
        fetchRequest.fetchLimit = 1
        var object: NSManagedObject? = .none
        context.performAndWait {
            do {
                object = try context.fetch(fetchRequest).first as? NSManagedObject
            } catch {
                print(error.localizedDescription)
            }
        }
        return object
    }
    
    class func findObjectsWith(_ context: NSManagedObjectContext, predicate: NSPredicate, error: Error?) -> [Any]? {
        let fetchRequest = createFetchRequest()
        fetchRequest.predicate = predicate
        fetchRequest.fetchLimit = 1
        var object: Any? = .none
        context.performAndWait {
            do {
                object = try context.fetch(fetchRequest)
            } catch {
                print(error.localizedDescription)
            }
        }
        return [object!]
    }
    
    class func createFetchRequest() -> NSFetchRequest<NSFetchRequestResult> {
        return NSFetchRequest(entityName: String(describing: self))
    }
}

extension UIColor {
    convenience init(hexString : String) {
        if let rgbValue = UInt(hexString, radix: 16) {
            let red   =  CGFloat((rgbValue >> 16) & 0xff) / 255
            let green =  CGFloat((rgbValue >> 8) & 0xff) / 255
            let blue  =  CGFloat((rgbValue) & 0xff) / 255
            self.init(red: red, green: green, blue: blue, alpha: 1)
        } else {
            self.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 1)
        }
    }
    
    open class var applicationGreenColor: UIColor {
        return UIColor(red: 116 / 255, green: 198 / 255, blue: 97 / 255, alpha: 1)
    }
    
    open class var applicationBlueColor: UIColor {
        return UIColor(red: 87 / 255, green: 129 / 255, blue: 181 / 255, alpha: 1)
    }
}

extension UserDefaults {
    
    private static let authorizationKey = "AuthorizationKey"
    
    class func didSaveAuthorizationStatus(_ isAuthorize: Bool) {
        UserDefaults.standard.set(isAuthorize, forKey: UserDefaults.authorizationKey)
        UserDefaults.standard.synchronize()
    }
    
    class func getAuthorizationStatus() -> Bool {
        return UserDefaults.standard.bool(forKey: UserDefaults.authorizationKey)
    }
    
    class func deleteAuthorizationStatus() {
        UserDefaults.standard.removeObject(forKey: UserDefaults.authorizationKey)
        UserDefaults.standard.synchronize()
    }
    
}
