//
//  OrdersTableViewCell.swift
//  remonline
//
//  Created by Рамиз Кичибеков on 03/08/2017.
//  Copyright © 2017 Рамиз Кичибеков. All rights reserved.
//

import UIKit

final class OrdersTableViewCell: UITableViewCell {

    @IBOutlet weak private var numberOrderLabel: UILabel!
    @IBOutlet weak private var deviceLabel: UILabel!
    @IBOutlet weak private var faultDeviceLabel: UILabel!
    @IBOutlet weak private var orderStatusView: UIView!
    
    open class var reuseIdentifier: String {
        return "OrderCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    open func configureCell(_ model: OrderEntity) {
        numberOrderLabel.text = model.numberOrder
        deviceLabel.text = model.device
        faultDeviceLabel.text = model.faultDevice
        orderStatusView.backgroundColor = configureStatusView(model.statusOrder as! Data)
    }
    
    private func configureStatusView(_ statusData: Data) -> UIColor {
        let status = NSKeyedUnarchiver.unarchiveObject(with: statusData) as! [String : Any]
        var hexColor = status["color"] as! String
        hexColor.remove(at: hexColor.startIndex)
        return UIColor(hexString: hexColor)
    }
    
}
