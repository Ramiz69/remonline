//
//  MainViewController.swift
//  remonline
//
//  Created by Рамиз Кичибеков on 23/07/2017.
//  Copyright © 2017 Рамиз Кичибеков. All rights reserved.
//

import UIKit
import CoreData

final class MainViewController: UITableViewController, NSFetchedResultsControllerDelegate, UISearchResultsUpdating, UISearchControllerDelegate {
    
    private lazy var fetchedResultsController = { () -> NSFetchedResultsController<OrderEntity> in
        let context = DataStore.localDataBase.managedObjectContext
        let entity = NSEntityDescription.entity(forEntityName: "OrderEntity", in: context)
        let sortDescription = NSSortDescriptor(key: "numberOrder", ascending: false)
        let request = NSFetchRequest<OrderEntity>()
        request.entity = entity
        request.sortDescriptors = [sortDescription]
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request,
                                                                  managedObjectContext: context,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
    }()
    
    @IBOutlet weak private var infinityScrollView: UIView!
    @IBOutlet weak private var infinityScrollIndicator: UIActivityIndicatorView!
    private lazy var searchController = { () -> UISearchController in
        let searchController = UISearchController(searchResultsController: nil)
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        searchController.definesPresentationContext = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Поиск"
        searchController.searchBar.scopeButtonTitles = ["Устройство", "Неисправность"]
        
        return searchController
    }()
    
    private var isLoadMore = true
    
    private var orderCountOnPage = 50
    private var currentPage = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    //MARK: - Custom method's
    
    private func setupController() {
        tableView.register(UINib.init(nibName: String(describing: OrdersTableViewCell.self), bundle: nil),
                           forCellReuseIdentifier: OrdersTableViewCell.reuseIdentifier)
        getOrderList()
        initialFetchedController()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        configureRefresh()
        tableView.tableHeaderView = searchController.searchBar
    }
    
    private func configureRefresh() {
        refreshControl?.addTarget(self, action: #selector(getOrderList), for: .valueChanged)
        refreshControl?.tintColor = view.tintColor
    }
    
    private func initialFetchedController() {
        try! fetchedResultsController.performFetch()
    }
    
    @objc private func getOrderList() {
        if !(refreshControl?.isRefreshing)! {
            refreshControl?.beginRefreshing()
        }
        RemonlineService.shared.getOrderList { (orders, error) in
            self.refreshControl?.endRefreshing()
            if let error = error {
                self.errorAlertWith(error.localizedDescription,
                                    actionBlock: { (alertController) in
                                        let agreeAction = UIAlertAction(title: "ОК",
                                                                        style: .default,
                                                                        handler: nil)
                                        alertController.addAction(agreeAction)
                })
            } else {
                self.initialFetchedController()
            }
        }
    }
    
    private func showInfinityScroll() {
        isLoadMore = false
        tableView.tableFooterView = infinityScrollView
    }
    
    private func removeInfinityScroll() {
        isLoadMore = true
        tableView.tableFooterView = nil
    }
    
    func loadMoreOrders() {
        if isLoadMore {
            showInfinityScroll()
            RemonlineService.shared.getOrderList(NSNumber(value: currentPage), complectionHandler: { (orders, error) in
                self.removeInfinityScroll()
                if let error = error {
                    self.errorAlertWith(error.localizedDescription,
                                        actionBlock: { (alertController) in
                                            let agreeAction = UIAlertAction(title: "ОК",
                                                                            style: .default,
                                                                            handler: nil)
                                            alertController.addAction(agreeAction)
                    })
                } else {
                    let ordersArray = orders as! [OrderEntity]
                    self.currentPage += ordersArray.count == self.orderCountOnPage ? 1 : 0
                    print("CurrentPage = \(self.currentPage)")
                }
            })
        }
    }
    
    //MARK: - UITableViewDelegate & UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OrdersTableViewCell.reuseIdentifier, for: indexPath) as! OrdersTableViewCell
        
        cell.configureCell(fetchedResultsController.object(at: indexPath))
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - NSFetchedResultsControllerDelegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .automatic)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    //MAKR: - UISearchControllerDelegate
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchText = searchController.searchBar.text!
        let predicate = searchController.searchBar.selectedScopeButtonIndex == 0 ? NSPredicate(format: "device CONTAINS[cd] %@", searchText) : NSPredicate(format: "faultDevice CONTAINS[cd] %@", searchText)
        fetchedResultsController.fetchRequest.predicate = predicate
        initialFetchedController()
        tableView.beginUpdates()
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        tableView.endUpdates()
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        fetchedResultsController.fetchRequest.predicate = nil
        initialFetchedController()
        tableView.reloadData()
    }
    
}
